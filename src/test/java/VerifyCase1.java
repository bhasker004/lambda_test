
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class VerifyCase1 {
    // Please use your input value in xml file: runTest.xml parameter name data
    final String url="http://the-internet.herokuapp.com/horizontal_slider";
    static double value;
    WebDriver driver;

    @Parameters({"data"})
    @BeforeClass
    public void openHomePage(String data) {
        try{

            //Verify the number, get exception if not a valid number.
            value = Double.parseDouble(data);

            //Open site
            String path1 = System.getProperty("user.dir");
            //Here chromedriver used as for MAC 64 bit machine, add supported driver in resource folder if need to change
            System.setProperty("webdriver.chrome.driver", path1 + "/src/main/resources/chromedriver_mac");
            driver = new ChromeDriver();
            driver.get(url);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().window().maximize();

            //Checking page is navigated with the expected title
            String actual = driver.getTitle();
            String expected = "The Internet";
            Assert.assertEquals(actual, expected);

        }catch(Exception e)
        {
            driver.close();
            Assert.fail(String.valueOf(e));
        }
    }


    @Test
    public void HandleSlider() {
        try {
            String Slider_element = "//*[@class='sliderContainer']/input";
            WebElement slider = driver.findElement(By.xpath(Slider_element));
            Thread.sleep(1000);

            double minValue = Double.parseDouble(slider.getAttribute("min"));
            double maxValue = Double.parseDouble(slider.getAttribute("max"));
            double stepValue = Double.parseDouble(slider.getAttribute("step"));

            //validation of value as per feature requirement
            if (value > maxValue || value < minValue || value%stepValue!=0) {
                throw new Exception("Not a valid value as per feature : " + value);
            }

            //move slider to the give position
            for (int i = 0; i < value/stepValue; i++) {
                slider.sendKeys(Keys.RIGHT);
            }

            double actual=Double.parseDouble(driver.findElement(By.xpath("//*[@id='range']")).getText());
            System.out.println("Slider expected value : " + value);
            System.out.println("Slider actual value : " + actual);
            Assert.assertEquals(actual,value,"Slider actual value not match with expected value");

            Thread.sleep(1000);
            driver.close();

        }catch (Exception e){
            driver.close();
            Assert.fail(String.valueOf(e));
        }

    }
}