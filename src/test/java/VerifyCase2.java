import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class VerifyCase2 {

    WebDriver driver;
    final String product = "boat speaker";


    @BeforeClass
    public void openHomePage() {
        try{
            String path1 = System.getProperty("user.dir");
            System.setProperty("webdriver.chrome.driver", path1 + "/src/main/resources/chromedriver_mac");
            driver = new ChromeDriver();

            String url = "https://www.flipkart.com";
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(url);

            //Checking page is navigated with the expected title
            String actual = driver.getTitle();
            String expected = "Online Shopping Site for Mobiles, Electronics, Furniture, Grocery, Lifestyle, Books & More. Best Offers!";
            Assert.assertEquals(actual, expected);

            //Clicking on Cross button
            WebElement sign_in_cross = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']"));
            sign_in_cross.click();

        }catch(Exception e)
        {
            driver.close();
            Assert.fail(String.valueOf(e));
        }
    }



    @Test
    public void showProductAndPrice() throws InterruptedException {
        try {
            //Check Search Box is Displayed
            WebElement search = driver.findElement(By.xpath("//input[@name ='q' and @type='text']"));
            Assert.assertEquals(search.isDisplayed(), true);

            //Enter TV in Search text box
            search.sendKeys(product);

            //Clicked on Search button
            driver.findElement(By.xpath("//button[@class='L0Z3Pu']")).click();
            Thread.sleep(2000);

            Actions actions = new Actions(driver);
            Action PageDown = actions.sendKeys(Keys.PAGE_DOWN).build();
            PageDown.perform();
            Thread.sleep(1000);

            String page = "(//*[@class='yFHi8N']//a[starts-with(@class,'ge-49M')])";
            List<WebElement> pages = driver.findElements(By.xpath(page));
            for (int index = 1; index <= pages.size(); index++) {

                driver.findElement(By.xpath(page + "[" + index + "]")).click();
                PageDown = actions.sendKeys(Keys.PAGE_DOWN).build();
                PageDown.perform();
                Thread.sleep(1000);

                String items_list = "//*[@class='_13oc-S']/div";
                List<WebElement> listTotal = driver.findElements(By.xpath(items_list));

                String Item_Name;
                String Item_Price;
                for (int i = 1; i <= listTotal.size(); i++) {
                    Item_Name = "(//*[@class='_13oc-S']//div[@data-id]//a[@title])[" + i + "]";
                    Item_Price = "(//*[@class='_13oc-S']//div[@data-id]//a[@class='_8VNy32']//div//div[@class='_30jeq3'])[" + i + "]";
                    System.out.print(driver.findElement(By.xpath(Item_Name)).getText() + " : ");
                    //***   To get the full product name use below statement instead of above   ****
                    //System.out.print(driver.findElement(By.xpath(Item_Name)).getAttribute("title") + " : ");
                    System.out.println(driver.findElement(By.xpath(Item_Price)).getText());
                }
            }
            driver.close();
        }catch (Exception e){
            driver.close();
            Assert.fail(String.valueOf(e));
        }

    }
}