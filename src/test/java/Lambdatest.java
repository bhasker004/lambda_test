

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;

public class Lambdatest {

    public RemoteWebDriver driver = null;
    String username = "bhasker130";
    String accessKey = "VlTGfVg4nfLto3z9g2Fi6qA0hqNHA2Xzxx5pPzxm9wsRDbzUib";

    @BeforeTest
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platform", "Windows 10");
        capabilities.setCapability("browserName", "Chrome");
        capabilities.setCapability("version", "87.0"); // If this cap isn't specified, it will just get the any available one
        capabilities.setCapability("resolution","1024x768");
        capabilities.setCapability("build", "First Test");
        capabilities.setCapability("name", "Sample Test");
        capabilities.setCapability("network", true); // To enable network logs
        capabilities.setCapability("visual", true); // To enable step by step screenshot
        capabilities.setCapability("video", true); // To enable video recording
        capabilities.setCapability("console", true); // To capture console logs

        try {
            driver= new RemoteWebDriver(new URL("https://"+username+":"+accessKey+"@hub.lambdatest.com/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            System.out.println("Invalid grid URL");
        }
    }

    @Test(enabled = true)
    public void testScript() throws Exception {
        try {
            /*driver.get("https://lambdatest.github.io/sample-todo-app/");
            driver.findElement(By.name("li1")).click();
            driver.findElement(By.name("li2")).click();
            driver.findElement(By.id("sampletodotext")).clear();
            driver.findElement(By.id("sampletodotext")).sendKeys("Yey, Let's add it to list");
            driver.findElement(By.id("addbutton")).click();*/

            double value = Double.parseDouble("3");
            driver.get("http://the-internet.herokuapp.com/horizontal_slider");
            driver.manage().window().maximize();

            //Checking page is navigated with the expected title
            String actual1 = driver.getTitle();
            String expected = "The Internet";
            Assert.assertEquals(actual1, expected);

            String Slider_element = "//*[@class='sliderContainer']/input";

            WebDriverWait wait = new WebDriverWait(driver, 40);
            WebElement slider = driver.findElement(By.xpath(Slider_element));
            wait.until(ExpectedConditions.visibilityOf(slider));

            double minValue = Double.parseDouble(slider.getAttribute("min"));
            double maxValue = Double.parseDouble(slider.getAttribute("max"));
            double stepValue = Double.parseDouble(slider.getAttribute("step"));

            //validation of value as per feature requirement
            if (value > maxValue || value < minValue || value%stepValue!=0) {
                throw new Exception("Not a valid value as per feature : " + value);
            }

            //move slider to the give position
            for (int i = 0; i < value/stepValue; i++) {
                slider.sendKeys(Keys.RIGHT);
            }

            double actual=Double.parseDouble(driver.findElement(By.xpath("//*[@id='range']")).getText());
            System.out.println("Slider expected value : " + value);
            System.out.println("Slider actual value : " + actual);
            Assert.assertEquals(actual,value,"Slider actual value not match with expected value");

            Thread.sleep(3000);
            driver.close();
            driver.quit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}